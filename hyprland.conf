# Execute your favorite apps at launch
exec-once = swww init
exec-once = waybar

monitor=,1920x1080@60,auto,1.0

source = ./custom/conf/binds.conf

misc {
enable_swallow = true
swallow_regex = ^(kitty|tmux)$
}

general {
    gaps_in = 5
    gaps_out = 7
    border_size = 1
    col.active_border =  $color6 $color8 45deg # or rgba(CC457fca) rgba(CC5691c8) 45deg
    col.inactive_border = rgba(595959aa) # or $color1
    layout = dwindle
}

decoration {
    rounding = 18
    blur {
        enabled = true
        size = 4
        passes = 2
	xray = yes
    }
    drop_shadow = no
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
  enabled = yes
  bezier = myBezier, 0.05, 0.9, 0.1, 1.05
  animation = windows, 1, 7, myBezier
  animation = windowsOut, 1, 7, default, popin 80%
  animation = border, 1, 10, default
  animation = borderangle, 1, 8, default
  animation = fade, 1, 7, default
  animation = workspaces, 1, 6, default
}

env = LIBVA_DRIVER_NAME,nvidia
env = XDG_SESSION_TYPE,wayland
env = GBM_BACKEND,nvidia-drm
env = __GLX_VENDOR_LIBRARY_NAME,nvidia
env = WLR_NO_HARDWARE_CURSORS,1
env = __GL_GSYNC_ALLOWED,1
windowrule = opacity 0.9, Thunar
windowrule = opacity 0.9, thunar
windowrule = opacity 0.9, wezterm

input {
    kb_layout = us
    follow_mouse = 1
    sensitivity = 0
}

dwindle {
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    new_is_master = true
}

gestures {
    workspace_swipe = off
}

bindel=, XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
bindel=, XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
bindl=, XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
